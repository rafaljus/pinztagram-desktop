﻿namespace Pinztagram.Contract
{
    public delegate void LoginEventHandler(bool logged, string message);

    public interface ILoginController
    {
        event LoginEventHandler LoginStateChanged;
        void Login();
        void CloseAppIfNotLogged();
    }
}
