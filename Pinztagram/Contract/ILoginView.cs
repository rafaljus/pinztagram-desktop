﻿namespace Pinztagram.Contract
{
    public interface ILoginView
    {
        string Username { get; set; }
        string Password { get; set; }

        void SetController(ILoginController loginController);
        void CloseApp();
    }
}
