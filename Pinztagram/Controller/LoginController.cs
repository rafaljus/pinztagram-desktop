﻿using System;
using Pinztagram.Contract;
using Pinztagram.DTO;
using Pinztagram.Model;
using Pinztagram.Service;

namespace Pinztagram.Controller
{
    public class LoginController : ILoginController
    {
        public event LoginEventHandler LoginStateChanged;

        private readonly ILoginView _view;

        public LoginController(ILoginView view)
        {
            view.SetController(this);
            this._view = view;
        }

        public async void Login()
        {
            var usernameValidateResult = ValidateUsername(_view.Username);
            if (usernameValidateResult.IsSuccess)
            {
                LoginStateChanged.Invoke(false, usernameValidateResult.Message);
                return;
            }

            var passwordValidateResult = ValidatePassword(_view.Password);
            if (passwordValidateResult.IsSuccess)
            {
                LoginStateChanged.Invoke(false, passwordValidateResult.Message);
                return;
            }

            var response = await LoginService.Login(_view.Username, _view.Password);
            LoginStateChanged.Invoke(response.IsSuccess, response.Message);
        }
        public void CloseAppIfNotLogged()
        {
            if (!LoginService.IsLogged())
                _view.CloseApp();
        }

        private static Result ValidateUsername(string username)
        {
            return new Result()
            {
                IsSuccess = String.IsNullOrEmpty(username),
                Message = String.IsNullOrEmpty(username)? "Pole login jest puste.": ""
            };
        }

        private static Result ValidatePassword(string password)
        {
            return new Result()
            {
                IsSuccess = String.IsNullOrEmpty(password),
                Message = String.IsNullOrEmpty(password) ? "Pole hasło jest puste." : ""
            };
        }
    }
}
