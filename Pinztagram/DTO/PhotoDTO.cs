﻿using System;
using System.Collections.Generic;

namespace Pinztagram.DTO
{
    public class PhotoDto
    {
        public string UserLogin { get; set; }
        public string DesktopSessionGuid { get; set; }
        public byte[] FileContent { get; set; }
        public string FileName { get; set; }
        public string FileDescription { get; set; }
        public decimal LocationLongitude { get; set; }
        public decimal LocationLatitude { get; set; }
        public string LocationName { get; set; }
        public IList<String> Tags { get; set; }
    }
}