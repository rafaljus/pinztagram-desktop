﻿namespace Pinztagram.DTO
{
    public class UserDto
    {
        public string UserLogin { get; set; }
        public string PasswordHash { get; set; }
    }
}