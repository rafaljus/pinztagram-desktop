﻿using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Pinztagram.View;

namespace Pinztagram
{
    public partial class LocationPicker : Form
    {
        GMapMarker marker;
        GMapOverlay markersOverlay;
        PhotoForm mainWindow;
        public LocationPicker(PhotoForm mainWindow)
        {
            this.mainWindow = mainWindow;
            InitializeComponent();
        }

        private void confirmButton_Click(object sender, EventArgs e)
        {
            try
            {
                mainWindow.SetLocation();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wystąpił problem podczas ustawiania lokalizacji: " + ex.Message);
            }
            this.Close();
        }

        private void gMapControl1_Load(object sender, EventArgs e)
        {
            gMapControl1.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;
            gMapControl1.ShowCenter = false;
            markersOverlay = new GMapOverlay("markers");
            gMapControl1.Overlays.Add(markersOverlay);

            if (LocationUtils.Latitude != "" && LocationUtils.Longitude != "")
            {
                double lat = double.Parse(LocationUtils.Latitude);
                double lng = double.Parse(LocationUtils.Longitude);
                marker = new GMarkerGoogle(new
                                 GMap.NET.PointLatLng(lat, lng),
                                 GMarkerGoogleType.red_pushpin);
                gMapControl1.Position = new GMap.NET.PointLatLng(lat, lng);
                markersOverlay.Markers.Add(marker);
            }
            else
            {
                gMapControl1.SetPositionByKeywords("Warsaw, Poland");
            }

            
        }

        private void gMapControl1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                markersOverlay.Markers.Remove(marker);
                double lat = gMapControl1.FromLocalToLatLng(e.X, e.Y).Lat;
                double lng = gMapControl1.FromLocalToLatLng(e.X, e.Y).Lng;

                LocationUtils.Latitude = lat.ToString();
                LocationUtils.Longitude = lng.ToString();

                marker = new GMarkerGoogle(new
                                 GMap.NET.PointLatLng(lat, lng),
                                 GMarkerGoogleType.red_pushpin);
                markersOverlay.Markers.Add(marker);
            }
        }
    }
}
