﻿using ExifLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Pinztagram
{
    public class LocationUtils
    {
        const string APIkey = "AIzaSyBmxSv-_7UiSduahpId4abwRqQmKmH7_xQ";
        //AIzaSyALyhhlbcJy-nKDJcsiqOHapWANVyyDcN0         

        public static string Latitude = "";
        public static string Longitude = "";

        public static void GetLocationFromImageFile(string filepath)
        {
            Latitude = "";
            Longitude = "";
            double[] lat;
            double[] lng;
            try
            {
                using (ExifReader exifReader = new ExifReader(filepath))
                {
                    if (exifReader.GetTagValue<double[]>(ExifTags.GPSLatitude, out lat) &&
                    exifReader.GetTagValue<double[]>(ExifTags.GPSLongitude, out lng))
                    {
                        for (int i = 0; i < lat.Length; i++)
                        {
                            Latitude += lat[i].ToString().Replace(",", "");
                            if (i == 0)
                            {
                                Latitude += ",";
                            }
                        }
                        for (int i = 0; i < lng.Length; i++)
                        {
                            Longitude += lng[i].ToString().Replace(",", "");
                            if (i == 0)
                            {
                                Longitude += ",";
                            }
                        }
                    }
                }
            }
            catch (ExifLibException ex)
            {//nie ma exifów w obrazku
                Latitude = "";
                Longitude = "";
            }

        }
    }


    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class ObjForSctripting
    {
        public void SetLocation(string lat, string lng)
        {
            LocationUtils.Latitude = lat;
            LocationUtils.Longitude = lng;
        }
    }
}
