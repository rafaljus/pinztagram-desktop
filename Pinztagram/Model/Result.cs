﻿using System;

namespace Pinztagram.Model
{
    public class Result
    {
        public bool IsSuccess;
        public String Message;
    }
}
