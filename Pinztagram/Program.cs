﻿using Pinztagram.View;
using System;
using System.Windows.Forms;
using Pinztagram.Service;

namespace Pinztagram
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            LoginForm loginForm = new LoginForm();
            Application.Run(loginForm);

            PhotoForm mainForm = new PhotoForm();
            Application.Run(mainForm);
            Console.WriteLine(LoginService.GetToken());
        }
    }
}
