﻿using Pinztagram.DTO;
using Pinztagram.Model;
using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace Pinztagram.Service
{
    public class LoginService
    {
        private static readonly String TokenKey = "API_TOKEN";

        private static readonly String LoginKey = "API_LOGIN";

        private static readonly String LoginUrl = ConfigurationManager.AppSettings["API_URL"] + "/api/Desktop/AuthenticateUser";

        public static async Task<Result> Login(String userLogin, String userPassword)
        {
            var httpClient = new HttpClient();

            Result response;

            try
            {
                var result = await httpClient.PostAsJsonAsync(LoginUrl, new UserDto
                {
                    UserLogin = userLogin,
                    PasswordHash = userPassword
                });

                if (result.IsSuccessStatusCode)
                {
                    SaveLogin(userLogin);
                    SaveToken(result.Content.ReadAsStringAsync().Result.Substring(1, 36));
                }

                response = new Result()
                {
                    IsSuccess = result.IsSuccessStatusCode,
                    Message = result.Content.ReadAsStringAsync().Result
                };
            }
            catch
            {
                response = new Result()
                {
                    IsSuccess = false,
                    Message = "Some error occured"
                };
            }

            return response;
        }

        private static void SaveToken(String token)
        {
            ConfigurationManager.AppSettings.Set(TokenKey, token);
        }

        private static void SaveLogin(String login)
        {
            ConfigurationManager.AppSettings.Set(LoginKey, login);
        }

        public static bool IsLogged()
        {
            return GetToken() != null;
        }

        public static String GetToken()
        {
            return ConfigurationManager.AppSettings.Get(TokenKey);
        }

        public static String GetLogin()
        {
            return ConfigurationManager.AppSettings.Get(LoginKey);
        }
    }
}
