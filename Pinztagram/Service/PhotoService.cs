﻿using Pinztagram.DTO;
using Pinztagram.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace Pinztagram.Service
{
    public class PhotoService
    {
        private static readonly String PhotoUrl = ConfigurationManager.AppSettings["API_URL"] + "/api/Desktop/SavePhoto";

        public static async Task<Result> SavePhoto(string userLogin, string token,
            byte[] fileContent, string fileName, string fileDescription, decimal locationLongitude, decimal locationLatitude, string locationName, IList<String> tags)
        {
            var httpClient = new HttpClient();

            Result response;

            try
            {
                var result = await httpClient.PostAsJsonAsync(PhotoUrl, new PhotoDto()
                {
                    UserLogin = LoginService.GetLogin(),
                    DesktopSessionGuid = LoginService.GetToken(),
                    FileContent = fileContent,
                    FileDescription = fileDescription,
                    FileName = fileName,
                    LocationLatitude = locationLatitude,
                    LocationLongitude = locationLongitude,
                    LocationName = locationName,
                    Tags = tags
                });

                response = new Result()
                {
                    IsSuccess = result.IsSuccessStatusCode,
                    Message = result.Content.ReadAsStringAsync().Result
                };
            }
            catch
            {
                response =  new Result()
                {
                    IsSuccess = false,
                    Message = "Some error occured"
                };
            }

            return response;
        }
    }
}
