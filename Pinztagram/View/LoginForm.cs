﻿using Pinztagram.Contract;
using Pinztagram.Controller;
using System;
using System.Windows.Forms;

namespace Pinztagram.View
{
    public partial class LoginForm : Form, ILoginView
    {
        public string Username { get; set; }
        public string Password { get; set; }

        private ILoginController _controller;

        public LoginForm()
        {
            InitializeComponent();
        }

        public void SetController(ILoginController loginController)
        {
            this._controller = loginController;
        }

        public void LoginStateChanged(bool logged, string message)
        {
            if (!logged)
            {
                MessageBox.Show(message, @"Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Clean();
                return;
            }

           this.Close();
        }

        public void CloseApp()
        {
            Application.Exit();
        }

        private void Clean()
        {
            this.loginTextBox.Text = "";
            this.passwordTextBox.Text = "";
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            _controller = new LoginController(this);
            _controller.LoginStateChanged += LoginStateChanged;
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _controller.LoginStateChanged -= LoginStateChanged;
            _controller.CloseAppIfNotLogged();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            Username = loginTextBox.Text;
            Password = passwordTextBox.Text;
            this._controller.Login();
        }
    }
}
