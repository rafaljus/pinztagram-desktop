﻿namespace Pinztagram.View
{
    partial class PhotoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhotoForm));
            this.sendPhotoDescription = new System.Windows.Forms.Label();
            this.pictureBoxG = new System.Windows.Forms.PictureBox();
            this.tags = new System.Windows.Forms.TextBox();
            this.geoDataDescription = new System.Windows.Forms.Label();
            this.googleMapsButton = new System.Windows.Forms.Button();
            this.sendButton = new System.Windows.Forms.Button();
            this.tagsDescription = new System.Windows.Forms.Label();
            this.geoDataText = new System.Windows.Forms.Label();
            this.ListViewItem_StickersList = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.location = new System.Windows.Forms.Label();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.labelDescription = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.labelPlaceDescription = new System.Windows.Forms.Label();
            this.textBoxPlaceDescription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxG)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sendPhotoDescription
            // 
            this.sendPhotoDescription.AutoSize = true;
            this.sendPhotoDescription.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sendPhotoDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sendPhotoDescription.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.sendPhotoDescription.Location = new System.Drawing.Point(141, 9);
            this.sendPhotoDescription.Name = "sendPhotoDescription";
            this.sendPhotoDescription.Size = new System.Drawing.Size(134, 24);
            this.sendPhotoDescription.TabIndex = 0;
            this.sendPhotoDescription.Text = "Prześlij zdjęcie";
            this.sendPhotoDescription.Click += new System.EventHandler(this.sendPhotoDescription_Click);
            // 
            // pictureBoxG
            // 
            this.pictureBoxG.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxG.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxG.Name = "pictureBoxG";
            this.pictureBoxG.Size = new System.Drawing.Size(400, 399);
            this.pictureBoxG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxG.TabIndex = 1;
            this.pictureBoxG.TabStop = false;
            // 
            // tags
            // 
            this.tags.Location = new System.Drawing.Point(109, 527);
            this.tags.Name = "tags";
            this.tags.Size = new System.Drawing.Size(303, 20);
            this.tags.TabIndex = 2;
            // 
            // geoDataDescription
            // 
            this.geoDataDescription.AutoSize = true;
            this.geoDataDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.geoDataDescription.Location = new System.Drawing.Point(12, 594);
            this.geoDataDescription.Name = "geoDataDescription";
            this.geoDataDescription.Size = new System.Drawing.Size(114, 13);
            this.geoDataDescription.TabIndex = 3;
            this.geoDataDescription.Text = "Dane geograficzne";
            // 
            // googleMapsButton
            // 
            this.googleMapsButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("googleMapsButton.BackgroundImage")));
            this.googleMapsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.googleMapsButton.Location = new System.Drawing.Point(368, 594);
            this.googleMapsButton.Name = "googleMapsButton";
            this.googleMapsButton.Size = new System.Drawing.Size(44, 39);
            this.googleMapsButton.TabIndex = 4;
            this.googleMapsButton.UseVisualStyleBackColor = true;
            this.googleMapsButton.Click += new System.EventHandler(this.googleMapsButton_Click);
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(12, 639);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(400, 40);
            this.sendButton.TabIndex = 5;
            this.sendButton.Text = "Wyślij";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // tagsDescription
            // 
            this.tagsDescription.AutoSize = true;
            this.tagsDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tagsDescription.Location = new System.Drawing.Point(12, 530);
            this.tagsDescription.Name = "tagsDescription";
            this.tagsDescription.Size = new System.Drawing.Size(95, 13);
            this.tagsDescription.TabIndex = 7;
            this.tagsDescription.Text = "Wprowadź tagi:";
            // 
            // geoDataText
            // 
            this.geoDataText.AutoSize = true;
            this.geoDataText.Location = new System.Drawing.Point(12, 545);
            this.geoDataText.Name = "geoDataText";
            this.geoDataText.Size = new System.Drawing.Size(0, 13);
            this.geoDataText.TabIndex = 8;
            // 
            // ListViewItem_StickersList
            // 
            this.ListViewItem_StickersList.Location = new System.Drawing.Point(428, 45);
            this.ListViewItem_StickersList.Name = "ListViewItem_StickersList";
            this.ListViewItem_StickersList.Size = new System.Drawing.Size(156, 634);
            this.ListViewItem_StickersList.TabIndex = 6;
            this.ListViewItem_StickersList.UseCompatibleStateImageBehavior = false;
            this.ListViewItem_StickersList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ListViewItem_StickersList_MouseClick);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(50, 50);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBoxG);
            this.panel1.Location = new System.Drawing.Point(12, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 399);
            this.panel1.TabIndex = 9;
            // 
            // location
            // 
            this.location.AutoSize = true;
            this.location.Location = new System.Drawing.Point(12, 620);
            this.location.Name = "location";
            this.location.Size = new System.Drawing.Size(66, 13);
            this.location.TabIndex = 10;
            this.location.Text = "brak danych";
            // 
            // imageList2
            // 
            this.imageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList2.ImageSize = new System.Drawing.Size(256, 256);
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDescription.Location = new System.Drawing.Point(12, 488);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(80, 13);
            this.labelDescription.TabIndex = 13;
            this.labelDescription.Text = "Opis zdjęcia:";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(109, 476);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(303, 41);
            this.textBoxDescription.TabIndex = 14;
            // 
            // labelPlaceDescription
            // 
            this.labelPlaceDescription.AutoSize = true;
            this.labelPlaceDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPlaceDescription.Location = new System.Drawing.Point(12, 558);
            this.labelPlaceDescription.Name = "labelPlaceDescription";
            this.labelPlaceDescription.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelPlaceDescription.Size = new System.Drawing.Size(82, 13);
            this.labelPlaceDescription.TabIndex = 15;
            this.labelPlaceDescription.Text = "Opis miejsca:";
            // 
            // textBoxPlaceDescription
            // 
            this.textBoxPlaceDescription.Location = new System.Drawing.Point(109, 555);
            this.textBoxPlaceDescription.Name = "textBoxPlaceDescription";
            this.textBoxPlaceDescription.Size = new System.Drawing.Size(303, 20);
            this.textBoxPlaceDescription.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(12, 453);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Tytuł:";
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Location = new System.Drawing.Point(109, 450);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.Size = new System.Drawing.Size(303, 20);
            this.textBoxFileName.TabIndex = 18;
            // 
            // PhotoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 694);
            this.Controls.Add(this.textBoxFileName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxPlaceDescription);
            this.Controls.Add(this.labelPlaceDescription);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.location);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.geoDataText);
            this.Controls.Add(this.tagsDescription);
            this.Controls.Add(this.ListViewItem_StickersList);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.googleMapsButton);
            this.Controls.Add(this.geoDataDescription);
            this.Controls.Add(this.tags);
            this.Controls.Add(this.sendPhotoDescription);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "PhotoForm";
            this.ShowIcon = false;
            this.Text = "Pinztagram";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxG)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label sendPhotoDescription;
        private System.Windows.Forms.PictureBox pictureBoxG;
        private System.Windows.Forms.TextBox tags;
        private System.Windows.Forms.Label geoDataDescription;
        private System.Windows.Forms.Button googleMapsButton;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.Label tagsDescription;
        private System.Windows.Forms.Label geoDataText;
        private System.Windows.Forms.ListView ListViewItem_StickersList;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label location;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Label labelPlaceDescription;
        private System.Windows.Forms.TextBox textBoxPlaceDescription;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxFileName;
    }
}