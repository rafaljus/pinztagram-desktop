﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Pinztagram.Contract;
using Pinztagram.Service;
using System.Text;

namespace Pinztagram.View
{
    public partial class PhotoForm : Form, IPhotoView
    {
        #region stickers
        private static Image image_close;
        private static Image image_resize;
        private Point mouseloc;
        private Boolean allowResize;
        private Dictionary<PictureBox, PictureBox> resize_a = new Dictionary<PictureBox, PictureBox>();
        private Dictionary<PictureBox, PictureBox> resize_b = new Dictionary<PictureBox, PictureBox>();
        private Dictionary<PictureBox, PictureBox> close_a = new Dictionary<PictureBox, PictureBox>();
        private Dictionary<PictureBox, PictureBox> close_b = new Dictionary<PictureBox, PictureBox>();
        private List<PictureBox> pictures = new List<PictureBox>();
        private Image main_image;
        #endregion
        public PhotoForm()
        {
            InitializeComponent();
            addStickersToList();
            importImages();
        }

        private void importImages()
        {
            string closeImagePath;
            string resizeImagePath;   
            
            if (Directory.Exists(Application.StartupPath + @"\img")) {
                closeImagePath = Application.StartupPath + @"\img\zamknij.png";
                resizeImagePath = Application.StartupPath + @"\img\powieksz.png";    
            }
            else {
                closeImagePath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + @"\img\zamknij.png";
                resizeImagePath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + @"\img\powieksz.png";
            }

            PhotoForm.image_close = Image.FromFile(closeImagePath);
            PhotoForm.image_resize = Image.FromFile(resizeImagePath);
        }

        private void chooseImage(PictureBox pictureBox)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Title = "Open Image";
            dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                pictureBox.Image = Image.FromFile(dlg.FileName);
                main_image = pictureBox.Image;
                LocationUtils.GetLocationFromImageFile(dlg.FileName);
                //sendPhotoDescription.Visible = false;
            }
            dlg.Dispose();
            SetLocation();
        }

        private void sendPhotoDescription_Click(object sender, EventArgs e)
        {
            chooseImage(pictureBoxG);
            Clear();
        }
        #region load stickers to right panel
        private void addStickersToList()
        {
            string stickersPath = Application.StartupPath + @"\Stickers\";
            DirectoryInfo dir = new DirectoryInfo(stickersPath);

            // Jeżeli w katalogu z .exe nie ma folderu z naklejkami wczytuje je z projektu
            if (!dir.Exists) {
                dir = new DirectoryInfo(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + @"\Stickers\");
            } 

            foreach (FileInfo file in dir.GetFiles())
            {
                try
                {
                    this.imageList1.Images.Add(Image.FromFile(file.FullName));
                    this.imageList2.Images.Add(Image.FromFile(file.FullName));
                }
                catch
                {
                    Console.WriteLine("This is not an image file");
                }
            }
//            this.ListViewItem_StickersList.View = View.LargeIcon;
            this.imageList1.ImageSize = new Size(50, 50);
            this.ListViewItem_StickersList.LargeImageList = this.imageList1;

            for (int j = 0; j < this.imageList1.Images.Count; j++)
            {
                ListViewItem item = new ListViewItem();
                item.ImageIndex = j;
                this.ListViewItem_StickersList.Items.Add(item);
            }
        }
        #endregion

        private void ResizeClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            allowResize = true;
            mouseloc = e.Location;
        }

        private void closem(object sender, EventArgs e)
        {
            Clear(close_b[(sender as PictureBox)]);
            Draw(false);
        }

        private void ResizeEnter(object sender, EventArgs e)
        {
            allowResize = false;

        }

        private void ResizeUnclick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            allowResize = false;
        }

        private void ResizeMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (allowResize)
            {
                PictureBox b = resize_b[(sender as PictureBox)];
                b.Size = new Size(b.Width + e.X - mouseloc.X, b.Height + e.Y - mouseloc.Y);
                additionalImageDraw(b);
            }
            Draw(false);
        }

        public void Click(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                mouseloc = e.Location;
            allowResize = false;

        }

        private void Unclick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            allowResize = false;
        }

        private void Leave(object sender, EventArgs e)
        {
            allowResize = false;
        }
        private void ResizeLeave(object sender, EventArgs e)
        {
            allowResize = false;
        }
        public void Move(object sender, MouseEventArgs e)
        {
            pictureBoxG.Image = main_image;

            if (e.Button == System.Windows.Forms.MouseButtons.Left && allowResize == false)
            {
                PictureBox przesun = (sender as PictureBox);
                przesun.Location = new Point(e.X + przesun.Left - mouseloc.X, e.Y + przesun.Top - mouseloc.Y);
                additionalImageDraw(przesun);
            }
            Draw(false);
        }

        private async void sendButton_Click(object sender, EventArgs e)
        {
            Draw(true);

            ImageConverter imageToSend = new ImageConverter();
            byte[] xByte = (byte[])imageToSend.ConvertTo(pictureBoxG.Image, typeof(byte[]));

            decimal latitude, longitude;

            try
            {
                latitude = Convert.ToDecimal(LocationUtils.Latitude);
                longitude = Convert.ToDecimal(LocationUtils.Longitude);
            }
            catch
            {
                latitude = 0;
                longitude = 0;
            }

            var result = await PhotoService.SavePhoto(
                LoginService.GetLogin(), LoginService.GetToken(), xByte, textBoxFileName.Text, textBoxDescription.Text, latitude, longitude, textBoxPlaceDescription.Text, tags.Text.Split());

            if (!result.IsSuccess)
            {
                MessageBox.Show(result.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            MessageBox.Show(@"Wysłano", @"OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
            ClearPhotoData();
        }

        private void ClearPhotoData()
        {
            main_image = null;
            textBoxFileName.Text = "";
            textBoxDescription.Text = "";
            textBoxPlaceDescription.Text = "";
            tags.Text = "";
            location.Text = "brak danych";
            Draw(true);
        }

        public void Draw(Boolean end)
        {
            pictureBoxG.Image = main_image;
            Bitmap bmp = new Bitmap(pictureBoxG.Width, pictureBoxG.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            pictureBoxG.DrawToBitmap(bmp, new Rectangle(0, 0, pictureBoxG.Width, pictureBoxG.Height));
            PictureBox b = null;
            if (!end)
            {
                for (int i = 0; i < pictures.Count; i++)
                {
                    b = pictures[i];
                    int x = b.Location.X - pictureBoxG.Location.X;
                    int y = b.Location.Y - pictureBoxG.Location.Y;
                    try
                    {
                        b.DrawToBitmap(bmp, new Rectangle(x, y, b.Width, b.Height));
                    }

                    catch (Exception) //wyjscie poza granice obrazu poprawiamy przy 'drukowaniu'
                    { }
                    pictureBoxG.Image = bmp;
                }
            }
            else
            {
                for (int i = 0; i < pictures.Count; i++)
                {
                    b = pictures[i];
                    b.Controls.Clear();

                    int x = b.Location.X - pictureBoxG.Location.X;
                    int y = b.Location.Y - pictureBoxG.Location.Y;
                    try
                    {
                        b.DrawToBitmap(bmp, new Rectangle(x, y, b.Width, b.Height));
                    }
                    catch (Exception)
                    {
                        if (x < 0 && y >= 0)
                        {
                            Rectangle sourceRectangle = new Rectangle(new Point(-x, y), new Size(b.Width - x, b.Height));
                            Graphics g = Graphics.FromImage(b.Image);
                            g.DrawImage(bmp, 0, y, sourceRectangle, GraphicsUnit.Pixel);
                        }
                        else if (y < 0 && x >= 0)
                        {
                            Rectangle sourceRectangle = new Rectangle(new Point(x, -y), new Size(b.Width, b.Height - y));
                            Graphics g = Graphics.FromImage(b.Image);
                            g.DrawImage(bmp, x, 0, sourceRectangle, GraphicsUnit.Pixel);

                        }
                        else //x<0 && y<0
                        {
                            Rectangle sourceRectangle = new Rectangle(new Point(-x, -y), new Size(b.Width - x, b.Height - y));
                            Graphics g = Graphics.FromImage(b.Image);
                            g.DrawImage(bmp, 0, 0, sourceRectangle, GraphicsUnit.Pixel);
                        }
                    }
                    pictureBoxG.Image = bmp;
                }
                Clear();
                main_image = bmp;
            }
        }

        public void Clear()
        {
            while (pictures.Count != 0)
                Clear(pictures[0]);
        }
        public void Clear(PictureBox B)
        {
            PictureBox X = close_a[B];//maly x
            PictureBox R = resize_a[B];//zmnien rozmiar

            resize_b.Remove(R);
            resize_a.Remove(B);

            close_b.Remove(X);
            close_a.Remove(B);

            B.Controls.Clear();
            pictureBoxG.Controls.Remove(B);
            pictures.Remove(B);
        }

        private void ListViewItem_StickersList_MouseClick(object sender, MouseEventArgs e)
        {
            AddSticker();
        }
        public void AddSticker()
        {
            PictureBox pb = new PictureBox();

            if (ListViewItem_StickersList.SelectedItems.Count > 0)
            {
                pb.Image = imageList2.Images[ListViewItem_StickersList.SelectedItems[0].ImageIndex];
            }
            for (int j = 0; j < this.ListViewItem_StickersList.SelectedIndices.Count; j++)
            {
                this.ListViewItem_StickersList.Items[this.ListViewItem_StickersList.SelectedIndices[j]].Selected = false;
            }

            pictureBoxG.Controls.Add(pb);

            if (pictures.Count == 0)
                pb.Parent = pictureBoxG;
            else
                pb.Parent = pictures[pictures.Count - 1];

            pictureBoxG.Controls.Add(pb);
            pictures.Add(pb);
            pb.Location = new Point(0, 0);
            pb.Size = new Size(256, 256);
            pb.BackColor = Color.Transparent;
            pb.SizeMode = PictureBoxSizeMode.StretchImage;
            pb.BringToFront();

            pb.MouseDown += new System.Windows.Forms.MouseEventHandler(Click);
            pb.MouseUp += new System.Windows.Forms.MouseEventHandler(Unclick);
            pb.MouseMove += new System.Windows.Forms.MouseEventHandler(Move);
            pb.MouseLeave += new EventHandler(Leave);


            PictureBox resize = new PictureBox();
            pb.Controls.Add(resize);
            resize.Image = image_resize;
            resize.Location = new Point(pb.Left + pb.Width - image_resize.Width, pb.Top + pb.Height - image_resize.Height);
            resize.Size = new Size(image_close.Height, image_close.Width);
            resize.Cursor = Cursors.SizeNWSE;
            resize.Parent = pb;
            resize.BackColor = Color.Transparent;
            resize.SizeMode = PictureBoxSizeMode.Zoom;
            resize.BringToFront();
            resize.MouseDown += new System.Windows.Forms.MouseEventHandler(ResizeClick);
            resize.MouseUp += new System.Windows.Forms.MouseEventHandler(ResizeUnclick);
            resize.MouseEnter += new EventHandler(ResizeEnter);
            resize.MouseMove += new System.Windows.Forms.MouseEventHandler(ResizeMove);
            resize_a.Add(pb, resize);
            resize_b.Add(resize, pb);

            PictureBox closeb = new PictureBox();
            pb.Controls.Add(closeb);
            closeb.Image = image_close;
            closeb.Location = new Point(pb.Left + pb.Width - image_close.Width, pb.Top);
            closeb.Size = new Size(image_close.Height, image_close.Width);
            closeb.Cursor = Cursors.No;
            closeb.Parent = pb;
            closeb.BackColor = Color.Transparent;
            closeb.SizeMode = PictureBoxSizeMode.Zoom;
            closeb.BringToFront();
            closeb.MouseDown += new System.Windows.Forms.MouseEventHandler(closem);
            close_a.Add(pb, closeb);
            close_b.Add(closeb, pb);
        }
        private void additionalImageDraw(PictureBox X)
        {
            resize_a[X].Location = new Point(X.Width - image_resize.Width, X.Height - image_resize.Height);
            close_a[X].Location = new Point(X.Width - image_close.Width, 0);
        }

        public void SetLocation()
        {
            if (LocationUtils.Latitude != "" && LocationUtils.Longitude != "")
            {
                location.Text = LocationUtils.Latitude + "; " + LocationUtils.Longitude;
            }
            else
            {
                location.Text = "brak danych";
            }
        }

        private void googleMapsButton_Click(object sender, EventArgs e)
        {
            LocationPicker locationPicker = new LocationPicker(this);
            locationPicker.Show();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}
